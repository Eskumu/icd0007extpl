<?php

require_once 'lib/tpl.php';
require_once 'OrderLine.php';

$lines = file('data/order.txt');

$order_lines = [];
foreach ($lines as $line) {

    $parts = explode(';', trim($line));

    list($name, $price, $in_stock) = $parts;

    $price = floatval($price); // string to float
    $in_stock = $in_stock === 'true'; // string to boolean

    // create new object and add it to $order_lines list
    $product = new OrderLine($name, $price, $in_stock);
    array_push($order_lines, $product);
}

// print list of order line objects
foreach ($order_lines as $order_line) {
    printf('name: %s, price: %s; in stock: %s' . PHP_EOL,
        $order_line->productName,
        $order_line->price,
        $order_line->inStock ? 'true' : 'false');
}

