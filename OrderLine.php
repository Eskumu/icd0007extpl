<?php
/**
 * Created by PhpStorm.
 * User: pro
 * Date: 14.03.2018
 * Time: 14:24
 */

class OrderLine {
    public $productName;
    public $price;
    public $inStock;

    /**
     * OrderLine constructor.
     * @param $productName
     * @param $price
     * @param $inStock
     */
    public function __construct($productName, $price, $inStock) {
        $this->productName = $productName;
        $this->price = $price;
        $this->inStock = $inStock;
    }


}