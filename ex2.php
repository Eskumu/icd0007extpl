<?php

require_once 'lib/tpl.php';
require_once 'OrderLine.php';

$order_lines = [
    new OrderLine('Pen', 1, true),
    new OrderLine('Paper', 3, false),
    new OrderLine('Staples', 2, true)];
$current_year = date('Y');
$data = [
    '$order_lines' => $order_lines,
    '$current_year' => $current_year,
    '$product_list_template' => 'templates/ex2_sub_1.html',
    '$product_list_template2' => 'templates/ex2_sub_2.html'
];

print render_template('templates/ex2_main.html', $data);
